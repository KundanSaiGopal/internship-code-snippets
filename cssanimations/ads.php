<?php

include('include.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Manufacturers looking for Distributors</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="images/icons/icon-384x384.png" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/style.css">
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5d73ec27eb1a6b0be60b8227/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
      <style>
   

   .site-menu > li > a{ padding-left:20px !important; padding-right:20px !important;border-radius:20px;user-select:none;color:#ffffff !important;}
    .site-menu > li > a:hover {box-shadow:4px 4px 10px #121212, -4px -4px 10px #4a4a4a; }
  
	.icon-menu:before {
    color: white;
    }


  .box{
    width: 1200px;
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));
    grid-gap: 15px;
    margin: 0 auto;
  }
  .card{
    position: relative;
    width: 300px;
    height: 350px;
    background: #fff;
    margin: 0 auto;
    margin-bottom:10vh;
    border-radius: 4px;
    box-shadow:0 2px 10px rgba(0,0,0,.2);
  }
  .card:before,
  .card:after
  {
    content:"";
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border-radius: 4px;
    background: #fff;
    transition: 0.5s;
    z-index:-1;
  }
  .card:hover:before{
  transform: rotate(20deg);
  box-shadow: 0 2px 20px rgba(0,0,0,.2);
  }
  .card:hover:after{
  transform: rotate(10deg);
  box-shadow: 0 2px 20px rgba(0,0,0,.2);
  }
  .card .imgBx{
  position: absolute;
  top: 10px;
  left: 10px;
  bottom: 10px;
  right: 10px;
  background: #222;
  transition: 0.5s;
  z-index: 1;
  }
  
  .card:hover .imgBx
  {
    bottom: 80px;
  }

  .card .imgBx img{
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      object-fit: cover;
  }

  .card .details{
      position: absolute;
      left: 10px;
      right: 10px;
      bottom: 10px;
      height: 60px;
      text-align: center;
  }

  .card .details h2{
   margin: 0;
   padding: 0;
   font-weight: 600;
   font-size: 20px;
   color: #777;
   text-transform: uppercase;
  } 

  .card .details h2 span{
  font-weight: 500;
  font-size: 16px;
  color: #f38695;
  display: block;
  margin-top: 5px;
   } 
     
  </style>
  </head>

  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  
  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
   

    <header class="site-navbar py-2  bg-white site-navbar-target" role="banner" style="position:sticky;top:0px;background-color:#303030 !important;">

                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-3 col-md-3">
                            <p class="mb-0">
                                <span class="mr-3">
                                    <strong class="text-white" style="font-size:30px;">Yelefo.</strong>
                                </span>
                            </p>
                        </div>
                        <div class="col-9 col-md-9 d-none d-xl-block move-left-right">
                            <nav class="site-navigation position-relative text-right" role="navigation">
                                <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block" style ="">
                                    <li>
                                        <a onclick="window.location='index'" class="navig">Home</a>
                                    </li>
                                    <li>
                                        <a onclick="window.location='ads'" class="nav-link navig">Companies</a>
                                    </li>
                                    <li>
                                        <a onclick="window.location='FAQ'" class="navig">FAQ</a>
                                    </li>
                                    <li>
                                        <a onclick="window.location='hiring'" class="navig">Hiring</a>
                                    </li>
                                    <li>
                                        <a onclick="window.location='login'" class="navig">Login</a>
                                    </li>
                                    <li>
                                        <a onclick="window.location='register'" style=" box-shadow:4px 4px 10px #121212, -4px -4px 10px #4a4a4a; " class="navig register">Free Registration</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>


                        <div class="d-xl-none ml-md-0 py-3" style="position: absolute; top: 3px; right:10px;">
                            <a href="#" class="site-menu-toggle js-menu-toggle text-white">
                                <span class="icon-menu h3"></span>
                            </a>
                        </div>

                    </div>
                </div>

            </header>
    <div class="site-section">
      <div class="container">
        <div class="row">
            <div class="col-sm-12" style="margin-bottom:30px;text-align:center;">
              <h1>
                  <u>List of Companies Needed For Distribution</u>
              </h1>
            </div>
            <div class="col-sm-12" style="margin-bottom:50px;text-align:center;">
              <h4>
                    We have Listed only few of our manufacturers here and please register to view all the manufacturers who are in need of Distributors. This list keeps on updating, We recommend you to register with us to get daily updates.
              </h4>
            </div>
          <div class="box">
              <?php
                $sql ="SELECT * FROM ads_data WHERE status = 'Active' ORDER BY dateTime DESC";
                $rec_data = mysqli_query($conn, $sql);
                while ($ads = mysqli_fetch_assoc($rec_data))
                {
              ?>
            <div class="card" onclick="window.location='register'" style="cursor:pointer;">
              <div class="imgBx">
                  <img src="<?php echo 'user/company_profile/'.$ads['user_id'].'/ads/'.$ads['id'].'/'.$ads['image']; ?>" alt="<?php echo $ads['Firm_name']; ?>">
              </div>
              <div class="details">
                  <h2><?php echo $ads['Firm_name']; ?></h2>
              </div>
            </div>
          <?php } ?>
            
        </div>
        </div>
      </div>
    </div>
    
    <div style="position:fixed;width:50px;;top:200px;right:0px;height:300px;border-radius:20px 0 0 20px;">
        <a href="https://api.whatsapp.com/send?phone=+918111012000" target="_blank"><img src="images/whatsapp.png" width="80%" style="margin-bottom:20px;"></a>
        <a href="https://www.facebook.com/yelefo.co" target="_blank"><img src="images/facebook.png" width="80%" style="margin-bottom:20px;"></a>
        <a href="https://t.me/joinchat/AAAAAEuZoV9oIPk76TMbFgyelefo" target="_blank"><img src="images/telegram.png" width="80%" style="margin-bottom:20px;"></a>
        <a href="https://twitter.com/yelefo" target="_blank"><img src="images/twitter.png" width="80%"></a>
    </div>
    

    <a href="/register" class="py-5 d-block" style="background-color:#1A1514;">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md10"><h2 style="color:#ff6600;">Let's Get Started</h2></div>
        </div>
      </div>  
    </a>
    
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-3">
                <h2 class="footer-heading mb-4" style="color:#ff6600">Other Services, We Offer</h2>
                <ul class="list-unstyled">
                    <li style="color:#919193;">Web Designing</li>
                    <li style="color:#919193;">MSME Registration (Free)</li>
                  <li style="color:#919193;">GSTIN Registration</li>
                  <li style="color:#919193;">Digital Signature Certificates</li>
                  <li style="color:#919193;">Mobile Applications</li>
                  <li style="color:#919193;">Branding</li>
                  <li style="color:#919193;">Digital Marketing</li>
                  <li style="color:#919193;">Logo Design</li>
                  <li style="color:#919193;">BPO</li>
                  <li style="color:#919193;">Tender Intimation</li>
                  <li style="color:#919193;">Tender Uploading</li>
                </ul>
              </div>
              <div class="col-md-2 ml-auto">
                <h2 class="footer-heading mb-4" style="color:#ff6600">Site Map</h2>
                <ul class="list-unstyled">
                    <li><a href="index">Home</a></li>
                    <li><a href="ads">Companies</a></li>
                    <li><a href="FAQ">FAQ</a></li>
                    <li><a href="hiring">Hiring</a></li>
                    <li><a href="login">Login</a></li>
                    <li><a href="register">Register</a></li>
                </ul>
              </div>
              <div class="col-md-4 ml-auto">
                <h2 class="footer-heading mb-4" style="color:#ff6600">Company Details</h2>
                <ul class="list-unstyled" style="color:#919193">
                    <li><b>Cryptographic Solutions</b></li>
                    <li><b>Head Office:</b></li>
                    <li>No: 725, Natarajan street, K.K.Nagar,</br>Trichy, Tamil Nadu - 620 021</li>
                    <li><b>Branch Office</b></li>
                    <li>Chennai, Salem, Coimbatore</li>
                    <li><b>Email:</b> <a href="mailto:support@yelefo.com">support@yelefo.com<a></a></li>
                    <li><b>Contact Number:</b> <a href="tel://#" >+91 81110 12000</a></li>
                </ul>
              </div>
              <div class="col-md-3">
                <h2 class="footer-heading mb-4" style="color:#ff6600">Follow Us</h2>
                <a href="https://www.facebook.com/yelefo.co" target="_blank" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                <a href="https://twitter.com/yelefo" target="_blank" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                <a href="https://t.me/joinchat/AAAAAEuZoV9oIPk76TMbFg" target="_blank" class="p-2 pr-3"><span class="icon-telegram"></span></a>
                <a href="https://api.whatsapp.com/send?phone=+918111012000" target="_blank" class="p-2"><span class="icon-whatsapp"></span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>


  </div> <!-- .site-wrap -->

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/main.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.sticky.js"></script>
    <script>
$(window).ready(function(){
	
	$('.navig').click(function()
	{
		$(this).css('animation', 'button 0.1s linear 0s 2 alternate');
		$this = $(this);
		setTimeout(function(){
			$this.css('animation','none');
		},100);
	})
	
})

</script>


  </body>
</html>